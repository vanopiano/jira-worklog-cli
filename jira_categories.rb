# encoding: utf-8
require 'json'

class JiraCategories
  def initialize
    categories
  end

  def default
    categories['default']
  end

  def list
    categories['list']
  end

  private

  def categories
    @categories ||= begin
      JSON.parse(File.open(categories_file_path, "r:UTF-8", &:read))
    end
  rescue
    puts "Failed to read categories file from: #{categories_file_path}".red
    exit
  end

  def categories_file_path
    @categories_file_path ||= "#{Dir.home}/jira_logger_categories.json"
  end
end
