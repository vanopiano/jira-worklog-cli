#!/usr/bin/env ruby
# encoding: utf-8

require_relative './jira_logger.rb'
require_relative './jira_settings.rb'
require_relative './jira_categories.rb'

categories = JiraCategories.new
settings = JiraSettings.new
cli = settings.cli

settings.set_if_nil(:url, "Enter your Jira base URL:  (example: https://jira.mycompany.com/)")
settings.set_if_nil(:login, "Enter your Jira login:  ")
settings.set_if_nil(:password, "Enter your Jira password:  ", hidden: true)
settings.set_boolean_if_nil(:single_story_mode, "Do you want to use single-story mode? (y/n)")

if settings.get(:single_story_mode)
  settings.set_if_nil(:story, "Enter your story id:  ")
else
  settings.set_array_if_nil(:stories, "Enter stories (or a blank line to quit):")
end

if settings.get(:single_story_mode)
  story = settings.get(:story)
else
  cli.choose do |menu|
    menu.prompt = "Please choose the story to create worklog:  "

    menu.choices(*settings.get(:stories)) do |a|
      story = a
    end
  end
end

time = cli.ask('Enter time to log work: ') { |q| q.default = '8h' }

yesterday = false
cli.choose do |menu|
  menu.prompt = "Please choose date for the new worklog: |today|"

  menu.choices(:today, :yesterday) do |a|
    yesterday = :yesterday == a
  end
  menu.default = :today
end

category = categories.default

cli.choose do |menu|
  menu.prompt = "Please choose the story to create worklog:  |#{category}|"

  menu.choices(*categories.list) do |a|
    category = a
  end
  menu.default = category
end

comment = nil
loop do
  comment = cli.ask("Enter comment for the worklog:  ")
  break if !comment.to_s.empty?
end

if story && time
  log_options = { time: time, yesterday: yesterday, comment: comment, category: category }
  if JiraLogger.new(settings.settings.merge('story_id' => story)).log_time(log_options)
    puts 'Ok'.green
  else
    puts "Time wasn't commited".red
  end
end
