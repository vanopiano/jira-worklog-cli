# encoding: utf-8

require 'mechanize'
require 'time'
require 'uri'
require 'colorize'

class JiraLogger
  attr_accessor :url, :agent, :story_id, :worklog_page
  TIME_1_DAY = 24 * 60 * 60

  def initialize(options = {})
    @agent = Mechanize.new
    @login = options['login']
    @password = options['password']
    @url = options['url']
    @story_id = options['story_id']
    login
  end

  def log_time(options = {})
    return false unless worklog_page
    worklog_form = worklog_page.forms.find { |form| form.action == 'CreateWorklog.jspa' }
    return false unless worklog_form
    worklog_form['timeLogged'] = options[:time]
    worklog_form['category'] = options[:category]
    worklog_form['comment'] = options[:comment]
    worklog_form['startDate'] = prepare_date(worklog_form['startDate'], options[:yesterday])

    puts ("*" * 40).green
    puts "Story ID: ".green + "#{@story_id}"
    puts "Time:     ".green + "#{worklog_form['timeLogged']}"
    puts "Category: ".green + "#{worklog_form['category']}"
    puts "Comment:  ".green + "#{worklog_form['comment']}"
    puts "Date:     ".green + "#{worklog_form['startDate']}"
    puts ("*" * 40).green

    if HighLine.new.agree('Are you sure want to commit this time? (y/n):'.yellow)
      worklog_form.submit unless ENV['DRY_JIRA_RUN']
      true
    else
      false
    end
  end

  private

  def login
    login_page = agent.get(url + '/login.jsp')
    login_form = login_page.forms.find { |form| form.action == '/login.jsp' }
    if login_form
      login_form['os_username'] = @login
      login_form['os_password'] = @password
      login_form['os_destination'] = "/browse/#{story_id}"
      ticket_page = agent.submit(login_form)
      @worklog_page = worklog_from_ticket(ticket_page)
    end
  end

  def worklog_from_ticket(ticket_page)
    link = ticket_page.links.find do |link|
      link.href.to_s.include?('/secure/CreateWorklog!default.jspa?id=') && !link.href.to_s.include?('{0}')
    end
    return unless link && link.href
    agent.get(link.href)
  end

  def prepare_date(start_date, yesterday)
    current_jira_time = Time.parse(start_date)
    time = yesterday ? (current_jira_time - TIME_1_DAY) : current_jira_time
    time.strftime('%d/%h/%y %l:%M %p').gsub(/[[:space:]]+/, ' ')
  end
end
