# encoding: utf-8
require 'json'
require 'highline'

class JiraSettings
  attr_accessor :settings, :cli

  def initialize()
    @cli = HighLine.new
    @settings = read_settings
  end

  def set(key, value)
    settings[key.to_s] = value
    save_settings
  end

  def get(key)
    settings[key.to_s]
  end

  def set_if_nil(key, question_text, options = {})
    return if get(key)

    puts ("*" * 40).yellow
    loop do
      value = options[:hidden] ? cli.ask(question_text) { |q| q.echo = false } : cli.ask(question_text)
      unless value.to_s.empty?
        set(key, value)
        break
      end
    end
  end

  def set_boolean_if_nil(key, question_text)
    return unless get(key).nil?

    puts ("*" * 40).yellow
    value = cli.agree(question_text)
    set(key, value)
  end

  def set_array_if_nil(key, question_text)
    return if get(key)

    puts ("*" * 40).yellow
    values = cli.ask(question_text) { |q| q.gather = '' }
    set(key, values)
  end

  private

  def read_settings
    begin
      JSON.parse(File.open(settings_file_path, "r:UTF-8", &:read))
    rescue
      warn "Failed to read settings file from: #{settings_file_path}"
      {}
    end
  end

  def save_settings
    File.open(settings_file_path, "w:UTF-8") { |f| f.write(JSON.pretty_generate(settings)) }
  end

  def settings_file_path
    @settings_file_path ||= "#{Dir.home}/jira_logger.json"
  end
end
